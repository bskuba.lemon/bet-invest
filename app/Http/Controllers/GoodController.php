<?php

namespace App\Http\Controllers;

use App\Http\Requests\GoodRequest;
use App\Models\Good;

class GoodController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  GoodRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GoodRequest $request)
    {
        $good = Good::create([
            'title' => $request->title,
            'price' => $request->price,
            'user_id' => auth()->user()->id
        ]);
        return $good;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  GoodRequest  $request
     * @param  \App\Models\Good  $good
     * @return \Illuminate\Http\Response
     */
    public function update(GoodRequest $request, Good $good)
    {
        $good->update([
            'title' => $request->title,
            'price' => $request->price,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Good  $good
     * @return \Illuminate\Http\Response
     */
    public function destroy(Good $good)
    {
        $good->delete();
    }
}
