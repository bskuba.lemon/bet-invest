<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ValidateSignature
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $input = $request->except('signature');
        $signature = $this->getSignature($input);
        if($request->signature === $signature){
            return $next($request);
        } else {
            return response()->json(['message' => $signature], 401);
        }
    }


    /**
     * @param $input
     * @return string
     */
    protected function getSignature($input): string
    {
        ksort($input);
        $result = '';
        foreach ($input as $key => $item) {
            $result .= $key . '=' . $item;
            if ($key !== array_key_last($input)){
                $result .= '&';
            }
        }
        return md5($result . env('SECRET'));
    }
}
